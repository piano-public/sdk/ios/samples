# Piano SDK samples

## Sample for iOS

Clone the repository:

```sh
> git clone https://gitlab.com/piano-public/sdk/ios/samples.git
```

Go to the PianoMobile directory:

```sh
> cd samples/PianoMobile
```

Install dependencies:

```sh
> pod install
```

Open a project:

```sh
> open PianoMobile.xcworkspace
```